<?php

namespace Tests\Feature;

use App\Contracts\SearchServiceInterface;
use App\Models\Course;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Scout\Builder;
use Mockery;
use Tests\TestCase;

class CourseSearchTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group search
     */
    public function it_can_search_courses()
    {
        $mock = Mockery::mock(SearchServiceInterface::class);
        $mock->shouldReceive('search')
            ->withArgs(['search-string', Course::class])
            ->andReturn(new Builder(new Course(), 'string'));

        app()->instance(SearchServiceInterface::class, $mock);

        $response = $this->json('GET', route('courses.index'), [
            'search' => 'search-string',
        ]);

        $response->assertStatus(200);
    }
}
