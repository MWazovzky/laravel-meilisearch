<?php

namespace Tests\Feature;

use App\Contracts\SearchServiceInterface;
use App\Models\Profession;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Scout\Builder;
use Mockery;
use Tests\TestCase;

class ProfessionSearchTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group search
     */
    public function it_can_search_professions()
    {
        $mock = Mockery::mock(SearchServiceInterface::class);
        $mock->shouldReceive('search')
            ->withArgs(['search-string', Profession::class])
            ->andReturn(new Builder(new Profession, 'string'));

        app()->instance(SearchServiceInterface::class, $mock);

        $response = $this->json('GET', route('professions.index'), [
            'search' => 'search-string',
        ]);

        $response->assertStatus(200);
    }
}
