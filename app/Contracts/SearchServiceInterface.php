<?php


namespace App\Contracts;

use Laravel\Scout\Builder;

interface SearchServiceInterface
{
    public function search(string $query, string $model): Builder;
}
