<?php


namespace App\Services;

use App\Contracts\SearchServiceInterface;
use Laravel\Scout\Builder;
use MeiliSearch\Index;

class SearchService implements SearchServiceInterface
{
    public function search(string $q, string $model, array $options = []): Builder
    {
        return $model::search($q, function (Index $meilisearch, $query, $defaultOptions) use ($options) {
            return $meilisearch->search($query, $options + $defaultOptions);
        });
    }
}
