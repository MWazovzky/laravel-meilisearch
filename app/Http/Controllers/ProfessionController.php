<?php

namespace App\Http\Controllers;

use App\Contracts\SearchServiceInterface;
use App\Models\Profession;
use Illuminate\Http\Request;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SearchServiceInterface $searchService)
    {
        $attributes = $request->validate([
            'q' => 'sometimes|string',
        ]);

        $query = empty($attributes['q']) ?
            Profession::query() :
            $searchService->search($attributes['q'], Profession::class);

        return response()->json([
            'data' => $query->get(),
        ], 200);
    }
}
