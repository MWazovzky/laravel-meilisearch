# Laravel MeiliSearch Demo

## Create Index

```
php artisan scout:index professions
php artisan scout:index courses
```

## Import documents

```
php artisan scout:import "App\Models\Profession"
php artisan scout:import "App\Models\Course"
```

## Flush documents

```
php artisan scout:flush "App\Models\Profession"
php artisan scout:flush "App\Models\Course"
```

## Links

[Laravel Scout](https://laravel.com/docs/7.x/scout)
[MeiliSearch](https://docs.meilisearch.com/guides/introduction/quick_start_guide.html)
[meilisearch-laravel-scout](https://github.com/meilisearch/meilisearch-laravel-scout)
